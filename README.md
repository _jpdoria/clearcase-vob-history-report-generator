# About #
This script automates the creation of report for all ClearCase vobs.

# Usage #
```
#!bash
Usage: ./gen_vob_report.ksh <vob storage location> <email address>
Example: ./gen_vob_report.ksh gsfpvobs user@email.com
```
