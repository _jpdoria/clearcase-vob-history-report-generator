#!/usr/bin/ksh
#=============================================================================#
# Script:       VOB History Report Generator (gen_vob_report.ksh)             #
# Function:	This script aims to automate the creation of report for all #
#		ClearCase vobs.                                               #
# Author:       John Paul P. Doria                                            #
# Date Created: August 6, 2013                                                #
#=============================================================================#

VOB_STGLOC=$1
EMAIL=$2
DATE=`date +%m%d%y%H%M%S`
SCRIPT_DIR=/net/scmprd2/scm/spm/bin/ccreports
TEMPDIR=/home/vobadm/vob_history_reports_${DATE}

# Exits and removes temp files if the script
# received signals: 1, 2, 3, 9, and 15.

trap "echo \"Interrupted.\"; rm -fr $TEMPDIR temp_file_${DATE}.tar temp_file_${DATE}.tar.gz > /dev/null 2>&1; exit 1" 1 2 3 9 15

# Checks if user is vobadm.

if [ "$LOGNAME" != "vobadm" ]; then
        echo "Error: script must be executed using vobadm."
        exit 1
fi

# Creates a temporary working directory.

if [ ! -d $TEMPDIR ]; then
	mkdir $TEMPDIR
fi

# Scans ClearCase vobs depending on the user's choice of VOB Storage Location.

if [ -z $VOB_STGLOC ] || [ -z $EMAIL ]; then
	echo "Error: VOB storage location and email address are required."
	echo "Usage: ./gen_vob_report.ksh <vob storage location> <email address>"
	echo "Example: ./gen_vob_report.ksh gsfpvobs user@email.com"
	rm -fr $TEMPDIR temp_file_${DATE}.tar temp_file_${DATE}.tar.gz > /dev/null 2>&1
	exit 1
else
	echo "==================================================="
	echo " VOB History Report Generator (gen_vob_report.ksh) "
	echo "==================================================="
	VOBLIST=`cleartool lsvob | grep $VOB_STGLOC | sort | sed 's/^*[ \t]*\/ccvobs\///g' | awk '{print $1}'`
	for VOB in $VOBLIST; do
		echo ">>> Generating report for /ccvobs/$VOB... \c"
		$SCRIPT_DIR/vob_history_report.pl -q -vob "$VOB" -report Raw_Data -out $TEMPDIR/${VOB}_${DATE}.csv > /dev/null 2>&1
		if [ $? -eq 1 ]; then
			echo "FAILED!"
		else
			echo "DONE!"
		fi
	done
fi

# Compresses the temporary working directory then sends it to user's email address. Performs clean-up after sending the report.

VOB_STGLOC=`echo $VOB_STGLOC | tr [a-z] [A-Z]`
echo ">>> Compressing reports... \c"
tar -cf temp_file_${DATE}.tar $TEMPDIR && gzip -fr -9 temp_file_${DATE}.tar
if [ $? -eq 1 ]; then
	echo "FAILED!"
	rm -fr $TEMPDIR temp_file_${DATE}.tar temp_file_${DATE}.tar.gz > /dev/null 2>&1
	exit 1
else
	echo "DONE!"
fi

echo ">>> Sending reports to $EMAIL... \c"
uuencode temp_file_${DATE}.tar.gz vob_history_reports_${DATE}.tar.gz | mailx -s "VOB History Reports for $VOB_STGLOC" -r "vobadm" $EMAIL
if [ $? -eq 1 ]; then
	echo "FAILED!"
	rm -fr $TEMPDIR temp_file_${DATE}.tar temp_file_${DATE}.tar.gz > /dev/null 2>&1
	exit 1
else
	echo "DONE!"
	echo ">>> Exiting script... \c"
	sleep 2
	echo "Goodbye!"
	rm -fr $TEMPDIR temp_file_${DATE}.tar temp_file_${DATE}.tar.gz > /dev/null 2>&1
	exit 0
fi
